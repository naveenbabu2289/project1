const concat = (
  arr1: ReadonlyArray<number>,
  arr2: ReadonlyArray<number>
): ReadonlyArray<number> => {
  for (const e of arr2) {
    arr1 = [...arr1, e];
  }
  return arr1;
};
console.log(concat([2, 6, 9], [6, 9, 7]));
