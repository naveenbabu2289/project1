const zip = (arr1: ReadonlyArray<number>, arr2: ReadonlyArray<number>) => {
  const result = [];
  for (const e of arr1) {
    result.push([arr1[e], arr2[e]]);
  }
  return result;
};
console.log(zip([5, 6, 9, 8], [9, 8, 7, 4]));
