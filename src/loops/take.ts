const takeWhile = (
  arr: ReadonlyArray<number>,
  f: (x: number) => boolean
): ReadonlyArray<number> => {
  const r = [];
  for (const e of arr) {
    if (f(e)) {
      r.push(e);
    } else {
      break;
    }
  }
  return r;
};
console.log(takeWhile([2, 4, 6, 13], x => x % 2 === 0));
