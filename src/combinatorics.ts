import range from './range'
export const factorial=(n:number):number=>{
let fact=1;
for(const i of range(2,n+1)){
fact*=i;
}
return fact;
}
export default  factorial;
