const range = (start: number, stop: number): number[] => {
  const result: number[] = [];
  for (let i = start; i <= stop; ++i) {
    result.push(i);
  }
  return result;
};
const isprime = (n: number) => (factor(n).length === 2 ? true : false);
console.log(isprime(7));
const primenumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(n => isprime(n));
console.log(primenumbers(1, 50));
