const repeat = (x: number, n: number) => range(1, n).map(i => (i = x));
console.log(repeat(4, 3));

const power = (x: number, n: number) => repeat(x, n).reduce((a, b) => a * b);
console.log(power(2, 5));
