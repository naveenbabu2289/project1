const range = (start: number, stop: number): number[] => {
  const result: number[] = [];
  for (let i = start; i <= stop; ++i) {
    result.push(i);
  }
  return result;
};
const factor = (n: number) => range(1, n).filter(i => n % i === 0);
console.log(factor(28));
///////////
const perfect = (n: number) =>
  factor(n).reduce((x, y) => x + y) === 2 * n ? true : false;
console.log(perfect(6));

const perfectnumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(n => perfect(n));
console.log(perfectnumbers(1, 50));
