const max = (x: number, y: number): number => (x > y ? x : y);
const max2 = (arr: ReadonlyArray<number>) => arr.reduce(max);
console.log(max2([2, 6, 9, 8]));
